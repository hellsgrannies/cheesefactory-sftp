v0.1 -- Initial release.
v0.2 -- Removed extraneous files.
v0.3 -- Renamed folder from cheesefactory-sftp to cheesefactory_sftp.
v0.4 -- Updated README.
v0.5 -- Updated README.
v0.6 -- Removed misplaced __connect() in SFTP.__init__()
v0.7 -- 2018-09-29 Added regex to filter out files during walktree()
v0.8 -- 2018-10-01 Added callback for directory during walktree()
v0.9 -- 2018-10-05 Added is_dir() check during walktree()
v0.10 -- 2018-10-06 Fixed recursion problems for file/dir creation during walktree()
v0.11 -- 2018-10-10 Added ability to remove source file after successful get()
v0.12 -- 2018-10-10 Added ability to remove source file after successful get()
v0.13 -- 2018-10-16 Added ability to remove source file after successful get()
v0.14 -- 2019-01-27 Added put_new_files()
v0.15 -- 2019-06-25 Moved from re.match to re.search
v0.21 -- 2021-05-17 Major rewrite. Now based on Paramiko. Basic functionality.
v0.22 -- 2021-05-20 utilities.file_size_match() renamed to size_match() and now returns a bool. Expanded connection.log{}
v0.23 -- 2021-05-21 Added CfTransfer.log_checksum().
v0.24 -- 2021-05-22 Added transfer.transferred_local_files() and transferred_remote_files().
v0.27 -- 2021-05-23 Replaced append_local and append_remote with rename_local and rename_local.
v0.31 -- 2021-06-03 Fixed non-recursive list_dir() so it now correctly prepends directory name.
v0.32 -- 2021-06-06 Added _transfer() replace_local and replace_remote arguments.