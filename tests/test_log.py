# test/test_connection.py

import pytest
import logging
from cheesefactory_sftp.log import CfSftpLog, CfSftpLogTransfer

logger = logging.getLogger(__name__)


def test_cfsftplog():
    log = CfSftpLog()
    assert log.connection_ok is None
    assert log.host is None
    assert log.note is None
    assert log.password is None
    assert log.port is None
    assert log.transfers == []
    assert log.user is None


def test_cfsftplog_as():
    log = CfSftpLog()

    log.connection_ok = True
    log.host = '127.0.0.1'
    log.note = 'All is well.'
    log.password = 'iamapassword'
    log.port = '22'
    log.transfers = [
        {'action': 'ok', 'remote_path': '/usr/home/here/there.txt', 'size': 43421},
        {'action': 'ok', 'remote_path': '/usr/home/here3/there3.txt', 'size': 8654},
    ]
    log.user = 'harold'

    assert log.as_dict() == {
        'connection_ok': True,
        'host': '127.0.0.1',
        'note': 'All is well.',
        'password': 'iamapassword',
        'port': '22',
        'transfers': [
            {'action': 'ok', 'remote_path': '/usr/home/here/there.txt', 'size': 43421},
            {'action': 'ok', 'remote_path': '/usr/home/here3/there3.txt', 'size': 8654}
        ],
        'user': 'harold'}

    assert log.as_string() == "{'connection_ok': True, " \
                              "'host': '127.0.0.1', " \
                              "'note': 'All is well.', " \
                              "'password': 'iamapassword', " \
                              "'port': '22', " \
                              "'transfers': [" \
                              "{'action': 'ok', 'remote_path': '/usr/home/here/there.txt', 'size': 43421}, " \
                              "{'action': 'ok', 'remote_path': '/usr/home/here3/there3.txt', 'size': 8654}" \
                              "], " \
                              "'user': 'harold'}"


def test_cfsftplogtransfer():
    log = CfSftpLogTransfer()
    assert log.action is None
    assert log.action_ok is None
    assert log.client is None
    assert log.local_path is None
    assert log.note is None
    assert log.preserve_mtime is None
    assert log.preserve_mtime_ok is None
    assert log.remote_path is None
    assert log.remove_source is None
    assert log.remove_source_ok is None
    assert log.sha256_checksum is None
    assert log.size is None
    assert log.size_match is None
    assert log.size_match_ok is None
    assert log.status is None
    

def test_cfsftplogtransfer_as():
    log = CfSftpLogTransfer()
    log.action = 'GET'
    log.action_ok = True
    log.client = 'Bubba'
    log.local_path = '/here/is/local.txt'
    log.note = 'ok'
    log.preserve_mtime = True
    log.preserve_mtime_ok = False
    log.remote_path = '/here/is/remote.txt'
    log.remove_source = True
    log.remove_source_ok = False
    log.sha256_checksum = '23rouifhwoifhuweo4'
    log.size = 325235
    log.size_match = True
    log.size_match_ok = False
    log.status = 'yep'
    
    assert log.as_dict() == {
        'action': 'GET',
        'action_ok': True,
        'client': 'Bubba',
        'local_path': '/here/is/local.txt',
        'note': '/here/is/local.txt',
        'preserve_mtime': True,
        'preserve_mtime_ok': False,
        'remote_path': '/here/is/remote.txt',
        'remove_source': True,
        'remove_source_ok': False,
        'sha256_checksum': '23rouifhwoifhuweo4',
        'size': 325235,
        'size_match': True,
        'size_match_ok': False,
        'status': 'yep'
    }

    assert log.as_string() == "{" \
                              "'action': 'GET', " \
                              "'action_ok': True, " \
                              "'client': 'Bubba', " \
                              "'local_path': '/here/is/local.txt', " \
                              "'note': '/here/is/local.txt', " \
                              "'preserve_mtime': True, " \
                              "'preserve_mtime_ok': False, " \
                              "'remote_path': '/here/is/remote.txt', " \
                              "'remove_source': True, " \
                              "'remove_source_ok': False, " \
                              "'sha256_checksum': '23rouifhwoifhuweo4', " \
                              "'size': 325235, " \
                              "'size_match': True, " \
                              "'size_match_ok': False, " \
                              "'status': 'yep'" \
                              "}"