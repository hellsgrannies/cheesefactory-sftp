# test/test_utilities.py

import socket
import pytest
import time
import warnings
import logging
import paramiko
from cheesefactory_sftp.exceptions import CfSftpFileSizeMismatchError
from cheesefactory_sftp.utilities import CfSftpUtilities


logger = logging.getLogger(__name__)

#
# Test config
#
SFTP_USER = 'testingtom'
SFTP_PASSWORD = 'tomspassword'
SFTP_HOST = 'cfsftp_sshd'  # Container hostname in docker-compose.yml
SFTP_PORT = 22


def test_sftp_port_open():
    # Is port 22 open on the server? It should be.
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((SFTP_HOST, SFTP_PORT))
    assert result == 0, f'SFTP server ({SFTP_HOST}) not listening on port {str(SFTP_PORT)}'


@pytest.fixture(scope='module')
def sftp():
    cfsftp = CfSftpUtilities.connect(
        host=SFTP_HOST,
        port=SFTP_PORT,
        user=SFTP_USER,
        password=SFTP_PASSWORD
    )
    yield cfsftp

    logger.debug("Teardown CfSftp instance.")
    cfsftp.close()  # Needed to prepare fixture for next test run.


#
# Public Methods
#

@pytest.mark.parametrize('value', ['/', '/etc', '/home/testingtom'])
def test_chdir(sftp, value):
    # Change directory. Are we where we expect to be? We should be.
    sftp.chdir(value)
    assert sftp.cwd() == value


def test_chmod(sftp):
    # Does the remote file exist? It should.
    path = '/home/testingtom/chmod_test_file.txt'
    original_mode = 666
    new_mode = 700
    assert sftp.exists(path) is True

    # Get curent mod of remote file.
    stats = sftp.stat(path)
    assert sftp.st_mode_to_octal(stats.st_mode) == original_mode

    # Change file mode. Does it match the new mode? It should.
    sftp.chmod(path=path, mode=new_mode)
    stats = sftp.stat(path)
    assert sftp.st_mode_to_octal(stats.st_mode) == new_mode

    # Return the file mode to the original value. Did it change? It should have.
    sftp.chmod(path=path, mode=original_mode)
    stats = sftp.stat(path)
    assert sftp.st_mode_to_octal(stats.st_mode) == original_mode


def test_chown(sftp):
    # TODO: chown for uid not properly tested. Need to figure out permissions in container.
    # Does remote file exist? It should.
    remote_file = '/home/testingtom/chown_test_file.txt'
    assert sftp.exists(remote_file) is True

    # Get file UID and GID. Are they what we expect?
    stats = sftp.stat(remote_file)
    assert stats.st_uid == 1000
    assert stats.st_gid == 1000

    # Change file UID and GID. Did they change? They should have.
    sftp.chown(remote_file, uid=1000, gid=33)  # testingtom user and group
    stats = sftp.stat(remote_file)
    assert stats.st_uid == 1000
    assert stats.st_gid == 33

    # Return UID and GID to original values. Did they change? They should have.
    sftp.chown(remote_file, uid=1000, gid=1000)  # root user and group
    stats = sftp.stat(remote_file)
    assert stats.st_uid == 1000
    assert stats.st_gid == 1000


@pytest.mark.parametrize('value', ['/', '/etc', '/home/testingtom'])
def test_cwd(sftp, value):
    # Change directory. Does the current working directory match? It should.
    sftp.chdir(value)
    assert sftp.cwd() == value


def test_deprecation():
    sftp = CfSftpUtilities()
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("always")  # Cause all warnings to always be triggered.
        sftp.deprecation('This is a test')  # Trigger a warning.
        # Verify some things
        assert len(w) == 1
        assert issubclass(w[-1].category, DeprecationWarning)
        assert 'This is a test' in str(w[-1].message)


@pytest.mark.parametrize(
    'value,result',
    [('/etc', True), ('/etc/hosts', True), ('/home/testingtom', True), ('/home/testingtom/.bashrc', True),
     ('nothing_here', False), ('/nothere/either', False), ('/etc/norhere.txt', False)]
)
def test_exists(sftp, value, result):
    # Do these directories and files exist? The first two should. The last two shouldn't.
    assert sftp.exists(value) is result


def test_file_size_match_remote_size_none():
    """Remote size = None. Figure out the remote size and compare to local"""
    # Todo: Write this.
    pass


@pytest.mark.parametrize(
    'value,result',
    [('/etc', True), ('/etc/hosts', False), ('/home/testingtom', True), ('/home/testingtom/.bashrc', False)]
)
def test_is_dir(sftp, value, result):
    assert sftp.is_dir(value) is result


@pytest.mark.parametrize(
    'value,result',
    [('/etc', False), ('/etc/hosts', True), ('/home/testingtom', False), ('/home/testingtom/.bashrc', True)]
)
def test_is_file(sftp, value, result):
    assert sftp.is_file(value) is result


def test_list_dir(sftp):
    # Does the path exist? It should.
    path = '/home/testingtom'
    assert sftp.exists(path) is True

    # Does the directory list match what is expected?
    dir_list = sftp.list_dir(path)
    assert '/home/testingtom/.bashrc' in dir_list
    assert '/home/testingtom/.bash_logout' in dir_list
    assert '/home/testingtom/.profile' in dir_list
    assert '/home/testingtom/dir' in dir_list
    assert '/home/testingtom/general_test_file.txt' in dir_list


def test_list_dir_recursive(sftp):
    # Does the path exist? It should.
    path = '/home/testingtom'
    assert sftp.exists(path) is True

    # Does the directory list match what is expected?
    dir_list = sftp.list_dir(path, recursive=True)
    required_items = [
        '/home/testingtom/.bash_logout',
        '/home/testingtom/.bashrc',
        '/home/testingtom/.cache',
        '/home/testingtom/.cache/motd.legal-displayed',
        '/home/testingtom/.profile',
        '/home/testingtom/chmod_test_file.txt',
        '/home/testingtom/chown_test_file.txt',
        '/home/testingtom/dir',
        '/home/testingtom/dir/20200701_file_1.txt',
        '/home/testingtom/dir/20200701_file_2.txt',
        '/home/testingtom/dir/20200702_file_1.csv',
        '/home/testingtom/dir/20200702_file_2.csv',
        '/home/testingtom/dir/20200815_file_1.txt',
        '/home/testingtom/dir/20200815_file_2.txt',
        '/home/testingtom/dir/sub_dir1',
        '/home/testingtom/dir/sub_dir1/20200106_subdir1_file_2.txt',
        '/home/testingtom/dir/sub_dir1/20200106_subdir1_file_3.txt',
        '/home/testingtom/dir/sub_dir1/20200106_subdir1_file_4.csv',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_3.txt',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_4.csv',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
        '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_5.csv',
        '/home/testingtom/dir/sub_dir1/subdir1_file_1.csv',
        '/home/testingtom/dir/sub_dir1/subdir1_file_5.csv',
        '/home/testingtom/dir/sub_dir2',
        '/home/testingtom/dir/sub_dir2/20200106_subdir2_file_2.txt',
        '/home/testingtom/dir/sub_dir2/20200106_subdir2_file_3.txt',
        '/home/testingtom/dir/sub_dir2/20200106_subdir2_file_4.csv',
        '/home/testingtom/dir/sub_dir2/subdir2_file_1.csv',
        '/home/testingtom/dir/sub_dir2/subdir2_file_5.csv',
        '/home/testingtom/general_test_file.txt',
        '/home/testingtom/get_file_log_xfer_test_file.txt',
        '/home/testingtom/get_file_postfix_test_file.txt',
        '/home/testingtom/get_file_preserve_mtime_test_file.txt',
        '/home/testingtom/rename_test_file.txt',
        '/home/testingtom/stat_test_file.txt'
    ]
    for item in required_items:
        assert item in dir_list


@pytest.mark.parametrize('directory,mode', [('/home/testingtom/new_dir1', 700), ('/tmp/new_dir2', 775)])
def test_mkdir(sftp, directory, mode):
    # Does the new directory exist? It shouldn't
    if sftp.exists(directory):
        sftp.remove_dir(directory)
    assert sftp.exists(directory) is False

    # Make the new directory. Does it exist? It should.
    sftp.mkdir(directory, mode)
    assert sftp.exists(directory) is True

    # Does the new directory have the expected mode? It should.
    stats = sftp.stat(directory)
    assert sftp.st_mode_to_octal(stats.st_mode) == mode

    # Remove the directory. Does it exist? It shouldn't.
    sftp.remove_dir(directory)
    assert sftp.exists(directory) is False


@pytest.mark.parametrize(
    'octal,decimal',
    [(777, 511), (600, 384), (3432424, 931092), (0, 0)]
)
def test_octal_to_decimal(octal, decimal):
    sftp = CfSftpUtilities
    assert sftp.octal_to_decimal(octal) == decimal


@pytest.mark.parametrize('value', ['/home/testingtom/new_dir1', '/tmp/new_dir2'])
def test_remove_dir(sftp, value):
    # Does remote directory exist? It shouldn't.
    if sftp.exists(value) is True:
        sftp.remove_dir(value)
    assert sftp.exists(value) is False

    # Make the directory. Does it exist now? It should.
    sftp.mkdir(value)
    assert sftp.exists(value) is True

    # Remove the new directory. Is it gone? It should be.
    sftp.remove_dir(value)
    assert sftp.exists(value) is False


def test_remove_file(sftp):
    remote_path = '/home/testingtom/remove_test_file.txt'

    # Does the remote file exist? It shouldn't.
    if sftp.exists(remote_path):
        sftp.remove_file(remote_path)
    assert sftp.exists(remote_path) is False

    # Create the remote file. Does it exist now? It should.
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(SFTP_HOST, username=SFTP_USER, password=SFTP_PASSWORD)
    stdin, stdout, stderr = ssh.exec_command(f'touch {remote_path}')
    time.sleep(1)  # Slow the process down or sftp.exists() will not see the file!?
    logger.debug(f'\nstdin={stdin}\nstdout={stdout}\nstderr={stderr}')
    ssh.close()

    assert sftp.exists(remote_path) is True

    # Delete the remote file. Does it exist? It shouldn't.
    sftp.remove_file(remote_path)
    assert sftp.exists(remote_path) is False


def test_rename(sftp):
    # Does the remote file exist? It should.
    original_filename = '/home/testingtom/rename_test_file.txt'
    assert sftp.exists(original_filename) is True

    # Does the new remote file name exist? It shouldn't.
    new_filename = '/home/testingtom/new_file.txt'
    assert sftp.exists(new_filename) is False

    # Rename the remote flie. The old filename shouldn't exist. The new filename should.
    sftp.rename(original_filename, new_filename)
    assert sftp.exists(original_filename) is False
    assert sftp.exists(new_filename) is True

    # Rename the file back to the original name. The original name should exist. The new shouldn't.
    sftp.rename(new_filename, original_filename)
    assert sftp.exists(original_filename) is True
    assert sftp.exists(new_filename) is False


def test_size_match():
    sftp = CfSftpUtilities()
    # Do we get a FileSizeMismatchError when the file sizes do not match? We should.
    assert sftp.size_match(
        local_path='/etc/hosts', remote_path='/etc/hosts', remote_size=34
    ) is False

    # Do we get a FileSizeMismatchError when the file sizes match? We shouldn't.
    assert sftp.size_match(
        local_path='/etc/hosts', remote_path='/etc/hosts', remote_size=174
    ) is True


@pytest.mark.parametrize(
    'st_mode,octal',
    [(777, 411), (600, 130), (3432424, 750), (100, 144)]
)
def test_st_mode_to_octal(st_mode, octal):
    sftp = CfSftpUtilities
    assert sftp.st_mode_to_octal(st_mode) == octal


def test_stat(sftp):
    # Does the remote file exist? It should.
    filename = '/home/testingtom/stat_test_file.txt'
    assert sftp.exists(filename) is True

    # Do the file stats match what is expected? They should.
    file_stats = sftp.stat(filename)
    assert file_stats.FLAG_PERMISSIONS == 4
    assert file_stats.FLAG_SIZE == 1
    assert file_stats.FLAG_UIDGID == 2
    assert file_stats.st_gid == 1000
    assert file_stats.st_mode == 33206
    assert file_stats.st_size == 29
    assert file_stats.st_uid == 1000
