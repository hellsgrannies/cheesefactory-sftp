import pytest
from cheesefactory_sftp import exceptions


def test_cfsftperror():
    with pytest.raises(exceptions.CfSftpError) as excinfo:
        raise exceptions.CfSftpError('Testing CfSftpError.')
    assert 'Testing CfSftpError.' in str(excinfo.value)


def test_badlistvalueerror():
    with pytest.raises(exceptions.CfSftpBadListValueError) as excinfo:
        raise exceptions.CfSftpBadListValueError('Testing CfBadListValueError.')
    assert 'Testing CfBadListValueError.' in str(excinfo.value)


def test_emptylisterror():
    with pytest.raises(exceptions.CfSftpEmptyListError) as excinfo:
        raise exceptions.CfSftpEmptyListError(function_name='my_func', variable_name='my_var')
    assert 'Path list is empty. Function: my_func, Variable: my_var' in str(excinfo.value)


def test_filesizemismatcherror():
    with pytest.raises(exceptions.CfSftpFileSizeMismatchError) as excinfo:
        raise exceptions.CfSftpFileSizeMismatchError(
            local_path='/dir1', local_size='123', remote_path='/dir2', remote_size=2
        )
    assert 'Size mismatch -- Local: /dir1 (123) != Remote: /dir2 (2)' in str(excinfo.value)


def test_internalvalueerror():
    with pytest.raises(exceptions.CfSftpInternalValueError) as excinfo:
        raise exceptions.CfSftpInternalValueError('Testing CfSftpInternalValueError.')
    assert 'Testing CfSftpInternalValueError.' in str(excinfo.value)

