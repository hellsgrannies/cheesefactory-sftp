# tests/test_transfer.py

import logging
import socket
from cheesefactory_sftp.transfer import CfSftpTransfer

logger = logging.getLogger(__name__)


def test_cfsftptransfer():
    transfer = CfSftpTransfer()
    local_host = socket.gethostbyname(socket.gethostname())

    assert transfer._new_file_count == 0
    assert transfer._new_dir_count == 0
    assert transfer._existing_file_count == 0
    assert transfer._existing_dir_count == 0
    assert transfer._regex_skip_count == 0

    assert transfer.host is None
    assert transfer.port is None
    assert transfer.user is None
    assert transfer.password is None
    assert transfer.key_path is None
    assert transfer.key_password is None

    assert transfer._private_key is None
    assert transfer._transport is None
    assert transfer.sftp is None


def test_sha256_checksum():
    transfer = CfSftpTransfer()
    checksum = transfer.sha256_checksum('/root/.bashrc')
    logger.debug(f'checksum: {checksum}')
    assert checksum == '41f1685d04031d88891dea1cd02d5154f8aa841119001a72017b0e7158159e23'


def test_transferred_files():
    log = {
        'sftp_connection_ok': True,
        'sftp_host': 'cfsftp_sshd',
        'sftp_note': None,
        'sftp_password': 'tomspassword',
        'sftp_port': 22,
        'sftp_transfers': [
            {
                'action': 'GET', 'action_ok': True, 'append_local': None, 'append_remote': None, 'client': 'CfSftp',
                'local_path': '/tmp/dir1/remote_get_file_rename.txt',
                'note': 'preserve_mtime not yet implemented for PUT', 'preserve_mtime': True, 'preserve_mtime_ok': True,
                'remote_path': './remote_get_file_rename.txt', 'remove_source': False, 'remove_source_ok': None,
                'renamed_local_path': '/etc/renamed1', 'renamed_remote_path': None, 'sha256_checksum': None, 'size': 174,
                'size_match': True, 'size_match_ok': True, 'status': 'OK'
            },
            {
                'action': 'GET', 'action_ok': True, 'append_local': None, 'append_remote': None, 'client': 'CfSftp',
                'local_path': '/tmp/dir1/get_file_postfix_test_file.txt',
                'note': 'preserve_mtime not yet implemented for PUT', 'preserve_mtime': True, 'preserve_mtime_ok': True,
                'remote_path': './get_file_postfix_test_file.txt', 'remove_source': False, 'remove_source_ok': None,
                'renamed_local_path': None, 'renamed_remote_path': 'renamed5/yo', 'sha256_checksum': None, 'size': 29,
                'size_match': True, 'size_match_ok': True, 'status': 'OK'
            },
            {
                'action': 'GET', 'action_ok': True, 'append_local': None, 'append_remote': None, 'client': 'CfSftp',
                'local_path': '/tmp/dir1/get_file_preserve_mtime_test_file.txt',
                'note': 'preserve_mtime not yet implemented for PUT', 'preserve_mtime': True, 'preserve_mtime_ok': True,
                'remote_path': './get_file_preserve_mtime_test_file.txt', 'remove_source': False,
                'remove_source_ok': None, 'renamed_local_path': None, 'renamed_remote_path': None,
                'sha256_checksum': None, 'size': 29, 'size_match': True, 'size_match_ok': True, 'status': 'OK'
            },
            {
                'action': 'GET', 'action_ok': True, 'append_local': None, 'append_remote': None, 'client': 'CfSftp',
                'local_path': '/tmp/dir1/get_file_log_xfer_test_file.txt',
                'note': 'preserve_mtime not yet implemented for PUT', 'preserve_mtime': True, 'preserve_mtime_ok': True,
                'remote_path': './get_file_log_xfer_test_file.txt', 'remove_source': False, 'remove_source_ok': None,
                'renamed_local_path': './3/renamed2', 'renamed_remote_path': None, 'sha256_checksum': None, 'size': 29,
                'size_match': True, 'size_match_ok': True, 'status': 'OK'
            }
        ],
        'sftp_user': 'testingtom'
    }

    transfer = CfSftpTransfer()
    assert transfer.transferred_local_files(log) == ['/etc/renamed1',
                                                     '/tmp/dir1/get_file_postfix_test_file.txt',
                                                     '/tmp/dir1/get_file_preserve_mtime_test_file.txt',
                                                     './3/renamed2']
    assert transfer.transferred_remote_files(log) == ['./remote_get_file_rename.txt',
                                                      'renamed5/yo',
                                                      './get_file_preserve_mtime_test_file.txt',
                                                      './get_file_log_xfer_test_file.txt']
