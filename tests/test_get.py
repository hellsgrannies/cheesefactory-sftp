# tests/test_get.py

from pathlib import Path
import pytest
import logging
import socket
import paramiko
from cheesefactory_sftp.get import CfSftpGet
from cheesefactory_sftp.transfer_list import CfSftpTransferList

# noinspection DuplicatedCode
logger = logging.getLogger(__name__)

local_host = socket.gethostbyname(socket.gethostname())

#
# Test config
#

SFTP_USER = 'testingtom'
SFTP_PASSWORD = 'tomspassword'
SFTP_HOST = 'cfsftp_sshd'  # Container hostname in docker-compose.yml
SFTP_PORT = 22


def test_sftp_port_open():
    # Is port 22 open on the server? It should be.
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((SFTP_HOST, SFTP_PORT))
    assert result == 0, f'SFTP server ({SFTP_HOST}) not listening on port {str(SFTP_PORT)}'


@pytest.fixture(scope='function')
def sftp():
    cfsftp = CfSftpGet.connect(
        host=SFTP_HOST,
        port=SFTP_PORT,
        user=SFTP_USER,
        password=SFTP_PASSWORD
    )
    yield cfsftp

    logger.debug("Teardown CfSftp instance.")
    cfsftp.close()  # Needed to prepare fixture for next test run.


def test_cfsftpget():
    get = CfSftpGet()

    assert get._new_file_count == 0
    assert get._new_dir_count == 0
    assert get._existing_file_count == 0
    assert get._existing_dir_count == 0
    assert get._regex_skip_count == 0

    assert get.host is None
    assert get.port is None
    assert get.user is None
    assert get.password is None
    assert get.key_path is None
    assert get.key_password is None

    assert get._private_key is None
    assert get._transport is None
    assert get.sftp is None


#
# PUBLIC METHODS
#

def test_get(sftp):
    def clean():
        local_path.unlink(missing_ok=True)
        assert not local_path.exists()

    remote_path = Path('/home/testingtom/get_file_log_xfer_test_file.txt')
    local_path = Path('/tmp/get_file_log_xfer_test_file.txt')

    clean()
    transfer_log = sftp.get(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=False,
                            remove_source=False)
    assert transfer_log == {
        'action': 'GET',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/tmp/get_file_log_xfer_test_file.txt',
        'note': '',
        'preserve_mtime': False,
        'preserve_mtime_ok': None,
        'remote_path': '/home/testingtom/get_file_log_xfer_test_file.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': None,
        'size': 29,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert local_path.exists()
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [{
            'action': 'GET',
            'action_ok': True,
            'client': 'CfSftp',
            'local_path': '/tmp/get_file_log_xfer_test_file.txt',
            'note': '',
            'preserve_mtime': False,
            'preserve_mtime_ok': None,
            'remote_path': '/home/testingtom/get_file_log_xfer_test_file.txt',
            'remove_source': False,
            'remove_source_ok': None,
            'sha256_checksum': None,
            'size': 29,
            'size_match': True,
            'size_match_ok': True,
            'status': 'OK',
        }],
        'user': 'testingtom'
    }
    clean()


def test_get_checksum(sftp):
    def clean():
        local_path.unlink(missing_ok=True)
        assert not local_path.exists()

    remote_path = Path('/home/testingtom/get_file_log_xfer_test_file.txt')
    local_path = Path('/tmp/get_file_log_xfer_test_file.txt')

    clean()
    transfer_log = sftp.get(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=False,
                            remove_source=False, log_checksum=True)
    assert transfer_log == {
        'action': 'GET',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/tmp/get_file_log_xfer_test_file.txt',
        'note': '',
        'preserve_mtime': False,
        'preserve_mtime_ok': None,
        'remote_path': '/home/testingtom/get_file_log_xfer_test_file.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': 'c1fb5f7ce6b51dc68ba64b2d4789d417654497d493ac70547e319c2e607c9ab9',
        'size': 29,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert local_path.exists()
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [
            {
                'action': 'GET',
                'action_ok': True,
                'client': 'CfSftp',
                'local_path': '/tmp/get_file_log_xfer_test_file.txt',
                'note': '',
                'preserve_mtime': False,
                'preserve_mtime_ok': None,
                'remote_path': '/home/testingtom/get_file_log_xfer_test_file.txt',
                'remove_source': False,
                'remove_source_ok': None,
                'sha256_checksum': 'c1fb5f7ce6b51dc68ba64b2d4789d417654497d493ac70547e319c2e607c9ab9',
                'size': 29,
                'size_match': True,
                'size_match_ok': True,
                'status': 'OK'
            }
        ],
        'user': 'testingtom'}
    clean()


def test_get_preserve_mtime(sftp):
    def clean():
        local_path.unlink(missing_ok=True)
        assert not local_path.exists()

    # Do the local and remote files exist? They should.
    remote_path = Path('/home/testingtom/get_file_preserve_mtime_test_file.txt')
    local_path = Path('./test_files/downloaded_get_file_preserve_mtime_test_file.txt')

    clean()

    # Is mtime preserved?
    remote_mtime = sftp.stat(str(remote_path)).st_mtime
    transfer_log = sftp.get(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=True,
                             remove_source=False)
    assert transfer_log == {
        'action': 'GET',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': 'test_files/downloaded_get_file_preserve_mtime_test_file.txt',
        'note': 'preserve_mtime not yet implemented for PUT',
        'preserve_mtime': True,
        'preserve_mtime_ok': True,
        'remote_path': '/home/testingtom/get_file_preserve_mtime_test_file.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': None,
        'size': 29,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert local_path.exists()
    assert remote_mtime == local_path.stat().st_mtime
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [{
            'action': 'GET',
            'action_ok': True,
            'client': 'CfSftp',
            'local_path': 'test_files/downloaded_get_file_preserve_mtime_test_file.txt',
            'note': 'preserve_mtime not yet implemented for PUT',
            'preserve_mtime': True,
            'preserve_mtime_ok': True,
            'remote_path': '/home/testingtom/get_file_preserve_mtime_test_file.txt',
            'remove_source': False,
            'remove_source_ok': None,
            'sha256_checksum': None,
            'size': 29,
            'size_match': True,
            'size_match_ok': True,
            'status': 'OK'
        }],
        'user': 'testingtom'
    }

    clean()


def test_get_remove_source(sftp):
    def clean():
        local_path.unlink(missing_ok=True)
        assert not local_path.exists()

    remote_path = Path('/home/testingtom/get_file_remove_source_test_file.txt')
    local_path = Path('./test_files/downloaded_get_file_remove_source_test_file.txt')

    clean()

    # Prep a paramiko connection to upload missing file and test that it disappears.
    p_transport = paramiko.transport.Transport((SFTP_HOST, 22))
    p_transport.connect(None, SFTP_USER, SFTP_PASSWORD, None)
    p_sftp = paramiko.SFTPClient.from_transport(p_transport)

    if not sftp.exists(str(remote_path)):
        p_sftp.put(localpath='/etc/hosts', remotepath=str(remote_path))  # Replace missing file
    p_sftp.close()
    assert sftp.exists(str(remote_path)) is True

    transfer_dict = sftp.get(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=True,
                             remove_source=True)
    assert transfer_dict['local_path'] == str(local_path)
    assert local_path.exists() is True
    assert not sftp.exists(str(remote_path))  # Test that the remote file was deleted
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [{
            'action': 'GET',
            'action_ok': True,
            'client': 'CfSftp',
            'local_path': 'test_files/downloaded_get_file_remove_source_test_file.txt',
            'note': 'preserve_mtime not yet implemented for PUT',
            'preserve_mtime': True,
            'preserve_mtime_ok': True,
            'remote_path': '/home/testingtom/get_file_remove_source_test_file.txt',
            'remove_source': True,
            'remove_source_ok': True,
            'sha256_checksum': None,
            'size': 174,
            'size_match': True,
            'size_match_ok': True,
            'status': 'OK'
        }],
        'user': 'testingtom'
    }

    clean()


@pytest.mark.parametrize(
    'kwargs,log',
    [
        (
            {
                'file_list': [
                    {
                        'src': '/home/testingtom/.bashrc',
                        'dst': '/tmp/files_by_list1/home/testingtom/.bashrc'
                    }
                ],
                'preserve_mtime': True,
                'remove_source': False
            },
            {
                'connection_ok': True,
                'host': 'cfsftp_sshd',
                'note': None,
                'password': 'tomspassword',
                'port': '22',
                'transfers': [
                    {
                        'action': 'GET',
                        'action_ok': True,
                        'client': 'CfSftp',
                        'local_path': '/tmp/files_by_list1/home/testingtom/.bashrc',
                        'note': 'preserve_mtime not yet implemented for PUT',
                        'preserve_mtime': True,
                        'preserve_mtime_ok': True,
                        'remote_path': '/home/testingtom/.bashrc',
                        'remove_source': False,
                        'remove_source_ok': None,
                        'sha256_checksum': None,
                        'size': 3771,
                        'size_match': True,
                        'size_match_ok': True,
                        'status': 'OK'
                    }
                ],
                'user': 'testingtom'}
        ),
        (
            {
                'file_list': [
                    {
                        'src': '.bashrc',
                        'dst': '/tmp/files_by_list2/.bashrc'
                    },
                    {
                        'src': '/home/testingtom/.bash_logout',
                        'dst':  '/tmp/files_by_list2/home/testingtom/.bash_logout'
                    },
                    {
                        'src': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                        'dst': '/tmp/files_by_list2/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt'
                    },
                    {
                        'src': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                        'dst': '/tmp/files_by_list2'
                               '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt'
                    },
                    {
                        'src': '/etc/hosts',
                        'dst': '/tmp/files_by_list2/etc/hosts'
                    }
                ],
                'preserve_mtime': True,
                'remove_source': False
            },
            {'connection_ok': True,
             'host': 'cfsftp_sshd',
             'note': None,
             'password': 'tomspassword',
             'port': '22',
             'transfers': [
                {
                     'action': 'GET',
                     'action_ok': True,
                     'client': 'CfSftp',
                     'local_path': '/tmp/files_by_list2/.bashrc',
                     'note': 'preserve_mtime not yet implemented for PUT',
                     'preserve_mtime': True,
                     'preserve_mtime_ok': True,
                     'remote_path': '.bashrc',
                     'remove_source': False,
                     'remove_source_ok': None,
                     'sha256_checksum': None,
                     'size': 3771,
                     'size_match': True,
                     'size_match_ok': True,
                     'status': 'OK'
                },
                {
                    'action': 'GET',
                    'action_ok': True,
                    'client': 'CfSftp',
                    'local_path': '/tmp/files_by_list2/home/testingtom/.bash_logout',
                    'note': 'preserve_mtime not yet implemented for PUT',
                    'preserve_mtime': True,
                    'preserve_mtime_ok': True,
                    'remote_path': '/home/testingtom/.bash_logout',
                    'remove_source': False,
                    'remove_source_ok': None,
                    'sha256_checksum': None,
                    'size': 220,
                    'size_match': True,
                    'size_match_ok': True,
                    'status': 'OK'},
                {'action': 'GET',
                 'action_ok': True,
                 'client': 'CfSftp',
                 'local_path': '/tmp/files_by_list2/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                 'note': 'preserve_mtime not yet implemented for PUT',
                 'preserve_mtime': True,
                 'preserve_mtime_ok': True,
                 'remote_path': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                 'remove_source': False,
                 'remove_source_ok': None,
                 'sha256_checksum': None,
                 'size': 16,
                 'size_match': True,
                 'size_match_ok': True,
                 'status': 'OK'},
                {'action': 'GET',
                 'action_ok': True,
                 'client': 'CfSftp',
                 'local_path': '/tmp/files_by_list2/'
                               'home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                 'note': 'preserve_mtime not yet implemented for PUT',
                 'preserve_mtime': True,
                 'preserve_mtime_ok': True,
                 'remote_path': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                 'remove_source': False,
                 'remove_source_ok': None,
                 'sha256_checksum': None,
                 'size': 16,
                 'size_match': True,
                 'size_match_ok': True,
                 'status': 'OK'},
                {'action': 'GET',
                 'action_ok': True,
                 'client': 'CfSftp',
                 'local_path': '/tmp/files_by_list2/etc/hosts',
                 'note': 'preserve_mtime not yet implemented for PUT',
                 'preserve_mtime': True,
                 'preserve_mtime_ok': True,
                 'remote_path': '/etc/hosts',
                 'remove_source': False,
                 'remove_source_ok': None,
                 'sha256_checksum': None,
                 'size': 174,
                 'size_match': True,
                 'size_match_ok': True,
                 'status': 'OK'}],
             'user': 'testingtom'}
        ),
        (
            {
                'file_list': [
                    {
                        'src': '/home/testingtom/.bashrc',
                        'dst': '/tmp/files_by_list3/home/testingtom/.bashrc'
                    }
                ],
                'preserve_mtime': True,
                'remove_source': False,
            },
            {'connection_ok': True,
             'host': 'cfsftp_sshd',
             'note': None,
             'password': 'tomspassword',
             'port': '22',
             'transfers': [{'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list3/home/testingtom/.bashrc',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '/home/testingtom/.bashrc',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 3771,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'}],
             'user': 'testingtom'}
        ),
        (
            {
                'file_list': [
                    {
                        'src': '.bashrc',
                        'dst': '/tmp/files_by_list4.bashrc',
                    },
                    {
                        'src': '/home/testingtom/.bash_logout',
                        'dst': '/tmp/files_by_list4/home/testingtom/.bash_logout',
                    },
                    {
                        'src': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                        'dst': '/tmp/files_by_list4/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                    },
                    {
                        'src': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                        'dst': '/tmp/files_by_list4'
                               '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                    },
                    {
                        'src': '/etc/hosts',
                        'dst': '/tmp/files_by_list4/etc/hosts'
                    }
                ]
            },
            {'connection_ok': True,
             'host': 'cfsftp_sshd',
             'note': None,
             'password': 'tomspassword',
             'port': '22',
             'transfers': [{'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list4/.bashrc',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '.bashrc',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 3771,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'},
                           {'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list4/home/testingtom/.bash_logout',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '/home/testingtom/.bash_logout',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 220,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'},
                           {'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list4'
                                          '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/subsubdir1_file_1.txt',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 16,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'},
                           {'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list4'
                                          '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '/home/testingtom/dir/sub_dir1/sub_sub_dir1/20190423_subsubdir1_file_2.txt',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 16,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'},
                           {'action': 'GET',
                            'action_ok': True,
                            'client': 'CfSftp',
                            'local_path': '/tmp/files_by_list4/etc/hosts',
                            'note': 'preserve_mtime not yet implemented for PUT',
                            'preserve_mtime': True,
                            'preserve_mtime_ok': True,
                            'remote_path': '/etc/hosts',
                            'remove_source': False,
                            'remove_source_ok': None,
                            'sha256_checksum': None,
                            'size': 174,
                            'size_match': True,
                            'size_match_ok': True,
                            'status': 'OK'}],
             'user': 'testingtom'}
        ),
    ]
)
def test_get_files_by_list(sftp, kwargs, log):
    def clean(file_list):
        for dst_file in file_list:
            file_path = Path(dst_file)
            logger.debug(f'Unlinking file: {str(file_path)}')
            file_path.unlink(missing_ok=True)  # Unlink the file
            assert not file_path.exists()

    # Make transfer list and clean

    transfer_list = CfSftpTransferList.import_list(kwargs['file_list'])  # Import the transfer list
    dst_list = transfer_list.dst_list
    src_list = transfer_list.src_list

    assert transfer_list.list == kwargs['file_list']
    clean(dst_list)

    # Get

    for index, src in enumerate(src_list):
        dst = dst_list[index]
        logger.debug(f'Getting file: {src} -> {dst}')

    transfer_log = sftp.get_by_list(**kwargs)

    # Tear dicts apart to make sure we are comparing the correct things
    sftp_log_dict = sftp.log.as_dict()
    sftp_log_dict_transfers = sftp_log_dict['transfers']
    logger.debug(f'sftp_log_dict ({type(sftp_log_dict)}): {sftp_log_dict}')
    logger.debug(f'sftp_log_dict_transfers ({type(sftp_log_dict_transfers)}): {sftp_log_dict_transfers}')
    logger.debug(f'transfer_log ({type(transfer_log)}): {transfer_log}')

    # Compare file transfer parts of dictionaries
    for index, sftp_log_dict_transfer in enumerate(sftp_log_dict_transfers):
        logger.debug(
            f'Comparing:\n'
            f'({type(sftp_log_dict_transfer)}) {sftp_log_dict_transfer}\n'
            f'({type(transfer_log[index])}) {transfer_log[index]}'
        )
        assert sftp_log_dict_transfer == transfer_log[index]

    # Test files exist
    for file in dst_list:
        assert Path(file).exists()

    clean(dst_list)


# noinspection PyTypeChecker
def test_get_files_by_list_bad_file_list():
    get = CfSftpGet()

    with pytest.raises(ValueError) as excinfo:
        get.transfer_by_list(file_list='a string')
    assert 'file_list != List[dict] type.' in str(excinfo.value)

    with pytest.raises(ValueError) as excinfo:
        get.transfer_by_list(file_list=['file1', 'file2', 'file3'])
    assert 'file_list needs to contain only dictionaries.' in str(excinfo.value)

    with pytest.raises(ValueError) as excinfo:
        get.transfer_by_list(file_list=[
            {'src': 'src1.txt', 'dst': 'dst1.txt'}, {'src': 'src2.txt', 'dst': 'dst2.txt'},
            {'not_valid': 'src3.txt', 'dst': 'dst3.txt'}, {'src': 'src4.txt', 'dst': 'dst4.txt'}
        ])
    assert "A dict in the file_list is missing a 'src' or 'dst' key." in str(excinfo.value)
