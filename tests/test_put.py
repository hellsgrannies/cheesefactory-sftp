# tests/test_get.py

from pathlib import Path
import pytest
import logging
import glob
import socket
import paramiko
from cheesefactory_sftp.put import CfSftpPut
from cheesefactory_sftp.transfer_list import CfSftpTransferList

logger = logging.getLogger(__name__)

local_host = socket.gethostbyname(socket.gethostname())

#
# Test config
#

SFTP_USER = 'testingtom'
SFTP_PASSWORD = 'tomspassword'
SFTP_HOST = 'cfsftp_sshd'  # Container hostname in docker-compose.yml
SFTP_PORT = 22


def test_sftp_port_open():
    # Is port 22 open on the server? It should be.
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((SFTP_HOST, SFTP_PORT))
    assert result == 0, f'SFTP server ({SFTP_HOST}) not listening on port {str(SFTP_PORT)}'


@pytest.fixture(scope='function')
def sftp():
    cfsftp = CfSftpPut.connect(
        host=SFTP_HOST,
        port=SFTP_PORT,
        user=SFTP_USER,
        password=SFTP_PASSWORD
    )
    yield cfsftp

    logger.debug("Teardown CfSftp instance.")
    cfsftp.close()  # Needed to prepare fixture for next test run.


def test_cfsftpput():
    put = CfSftpPut()

    assert put._new_file_count == 0
    assert put._new_dir_count == 0
    assert put._existing_file_count == 0
    assert put._existing_dir_count == 0
    assert put._regex_skip_count == 0

    assert put.host is None
    assert put.port is None
    assert put.user is None
    assert put.password is None
    assert put.key_path is None
    assert put.key_password is None

    assert put._private_key is None
    assert put._transport is None
    assert put.sftp is None


#
# PUBLIC METHODS
#

def test_put(sftp):
    def clean():
        if sftp.exists(remote_path):
            sftp.remove(remote_path)
        assert not sftp.exists(remote_path)

    remote_path = '/tmp/hosts.txt'
    local_path = '/etc/hosts'

    clean()
    transfer_log = sftp.put(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=False,
                            remove_source=False)
    assert transfer_log == {
        'action': 'PUT',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/etc/hosts',
        'note': '',
        'preserve_mtime': False,
        'preserve_mtime_ok': None,
        'remote_path': '/tmp/hosts.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': None,
        'size': 174,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert sftp.exists(remote_path)
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [
            {
                'action': 'PUT',
                'action_ok': True,
                'client': 'CfSftp',
                'local_path': '/etc/hosts',
                'note': '',
                'preserve_mtime': False,
                'preserve_mtime_ok': None,
                'remote_path': '/tmp/hosts.txt',
                'remove_source': False,
                'remove_source_ok': None,
                'sha256_checksum': None,
                'size': 174,
                'size_match': True,
                'size_match_ok': True,
                'status': 'OK'
            }
        ],
        'user': 'testingtom'}
    clean()


def test_put_checksum(sftp):
    def clean():
        if sftp.exists(remote_path):
            sftp.remove(remote_path)
        assert not sftp.exists(remote_path)

    remote_path = '/tmp/hosts.txt'
    local_path = '/etc/hosts'

    clean()
    transfer_log = sftp.put(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=False,
                            remove_source=False, log_checksum=True)
    assert transfer_log == {
        'action': 'PUT',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/etc/hosts',
        'note': '',
        'preserve_mtime': False,
        'preserve_mtime_ok': None,
        'remote_path': '/tmp/hosts.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': '4c84357ba4eaecdbe8af3d4b8ffaeac4437be119a8159dad4d94db81c2b86684',
        'size': 174,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert sftp.exists(remote_path)
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': 'cfsftp_sshd',
        'note': None,
        'password': 'tomspassword',
        'port': '22',
        'transfers': [
            {
                'action': 'PUT',
                'action_ok': True,
                'client': 'CfSftp',
                'local_path': '/etc/hosts',
                'note': '',
                'preserve_mtime': False,
                'preserve_mtime_ok': None,
                'remote_path': '/tmp/hosts.txt',
                'remove_source': False,
                'remove_source_ok': None,
                'sha256_checksum': '4c84357ba4eaecdbe8af3d4b8ffaeac4437be119a8159dad4d94db81c2b86684',
                'size': 174,
                'size_match': True,
                'size_match_ok': True,
                'status': 'OK'
            }
        ],
        'user': 'testingtom'}
    clean()


def test_put_preserve_mtime(sftp):
    def clean():
        if sftp.exists(remote_path):
            sftp.remove(remote_path)
        assert not sftp.exists(remote_path)

    remote_path = '/tmp/hosts.txt'
    local_path = '/etc/hosts'

    clean()

    transfer_log = sftp.put(remote_path=str(remote_path), local_path=str(local_path), preserve_mtime=True,
                            remove_source=False)
    assert transfer_log == {
        'action': 'PUT',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/etc/hosts',
        'note': 'preserve_mtime not yet implemented for PUT',
        'preserve_mtime': True,
        'preserve_mtime_ok': True,
        'remote_path': '/tmp/hosts.txt',
        'remove_source': False,
        'remove_source_ok': None,
        'sha256_checksum': None,
        'size': 174,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }


def test_put_remove_source(sftp):
    def clean():
        if sftp.exists(remote_path):
            sftp.remove(remote_path)
        assert not sftp.exists(remote_path)
        Path(local_path).touch(exist_ok=True)
        assert Path(local_path).exists

    remote_path = '/tmp/remote_put_remove.txt'
    local_path = '/tmp/local_put_remove.txt'
    clean()

    transfer_log = sftp.put(local_path=local_path, remote_path=remote_path, remove_source=True)
    assert transfer_log == {
        'action': 'PUT',
        'action_ok': True,
        'client': 'CfSftp',
        'local_path': '/tmp/local_put_remove.txt',
        'note': 'preserve_mtime not yet implemented for PUT',
        'preserve_mtime': True,
        'preserve_mtime_ok': True,
        'remote_path': '/tmp/remote_put_remove.txt',
        'remove_source': True,
        'remove_source_ok': True,
        'sha256_checksum': None,
        'size': 0,
        'size_match': True,
        'size_match_ok': True,
        'status': 'OK'
    }
    assert sftp.exists(str(remote_path)) is True
    assert Path(local_path).exists() is False

    clean()


@pytest.mark.parametrize(
    'kwargs,log',
    [
        (
            {
                'file_list': [
                    {
                        'src': '/root/.bashrc',
                        # 'dst': '/tmp/files_by_list1/home/testingtom/.bashrc',
                        'dst': '/tmp/.bashrc',
                    }
                ],
                'preserve_mtime': True,
                'remove_source': False
            },
            {
                'connection_ok': True,
                'host': 'cfsftp_sshd',
                'note': None,
                'password': 'tomspassword',
                'port': '22',
                'transfers': [
                    {
                        'action': 'PUT',
                        'action_ok': True,
                        'client': 'CfSftp',
                        'local_path': '/root/.bashrc',
                        'note': 'preserve_mtime not yet implemented for PUT',
                        'preserve_mtime': True,
                        'preserve_mtime_ok': True,
                        'remote_path': '/tmp/.bashrc',
                        'remove_source': False,
                        'remove_source_ok': None,
                        'sha256_checksum': None,
                        'size': 570,
                        'size_match': True,
                        'size_match_ok': True,
                        'status': 'OK'
                    }
                ],
                'user': 'testingtom'
            }
        ),
        (
            {
                'file_list': [
                    {
                        'src': '/root/.bashrc',
                        'dst': '/tmp/.bashrc',
                    },
                    {
                        'src': '/etc/hosts',
                        'dst': '/tmp/hosts',
                    },
                    {
                        'src': '/etc/hostname',
                        'dst': '/tmp/hostname',
                    },
                    {
                        'src': '/etc/adduser.conf',
                        'dst': '/tmp/adduser.conf',
                    },
                ],
                'preserve_mtime': True,
                'remove_source': False
            },
            {
                'connection_ok': True,
                'host': 'cfsftp_sshd',
                'note': None,
                'password': 'tomspassword',
                'port': '22',
                'transfers': [
                    {'action': 'PUT',
                     'action_ok': True,
                     'client': 'CfSftp',
                     'local_path': '/root/.bashrc',
                     'note': 'preserve_mtime not yet implemented for PUT',
                     'preserve_mtime': True,
                     'preserve_mtime_ok': True,
                     'remote_path': '/tmp/.bashrc',
                     'remove_source': False,
                     'remove_source_ok': None,
                     'sha256_checksum': None,
                     'size': 570,
                     'size_match': True,
                     'size_match_ok': True,
                     'status': 'OK'},
                    {'action': 'PUT',
                     'action_ok': True,
                     'client': 'CfSftp',
                     'local_path': '/etc/hosts',
                     'note': 'preserve_mtime not yet implemented for PUT',
                     'preserve_mtime': True,
                     'preserve_mtime_ok': True,
                     'remote_path': '/tmp/hosts',
                     'remove_source': False,
                     'remove_source_ok': None,
                     'sha256_checksum': None,
                     'size': 174,
                     'size_match': True,
                     'size_match_ok': True,
                     'status': 'OK'},
                    {'action': 'PUT',
                     'action_ok': True,
                     'client': 'CfSftp',
                     'local_path': '/etc/hostname',
                     'note': 'preserve_mtime not yet implemented for PUT',
                     'preserve_mtime': True,
                     'preserve_mtime_ok': True,
                     'remote_path': '/tmp/hostname',
                     'remove_source': False,
                     'remove_source_ok': None,
                     'sha256_checksum': None,
                     'size': 13,
                     'size_match': True,
                     'size_match_ok': True,
                     'status': 'OK'},
                    {'action': 'PUT',
                     'action_ok': True,
                     'client': 'CfSftp',
                     'local_path': '/etc/adduser.conf',
                     'note': 'preserve_mtime not yet implemented for PUT',
                     'preserve_mtime': True,
                     'preserve_mtime_ok': True,
                     'remote_path': '/tmp/adduser.conf',
                     'remove_source': False,
                     'remove_source_ok': None,
                     'sha256_checksum': None,
                     'size': 2981,
                     'size_match': True,
                     'size_match_ok': True,
                     'status': 'OK'}
                ],
                'user': 'testingtom'
            }
        ),
    ]
)
def test_put_files_by_list(sftp, kwargs, log):
    def clean(c_file_list):
        for dst_file in c_file_list:
            logger.debug(f'Unlinking remote file: {dst_file}')
            sftp.remove_file(dst_file, missing_ok=True)  # Unlink the file
            assert not sftp.exists(dst_file)

    transfer_list = CfSftpTransferList.import_list(kwargs['file_list'])  # Import the transfer list
    dst_list = transfer_list.dst_list
    src_list = transfer_list.src_list
    assert transfer_list.list == kwargs['file_list']  # Test that the import worked
    clean(dst_list)

    # Put

    for index, src in enumerate(src_list):
        dst = dst_list[index]
        logger.debug(f'Putting file: {src} -> {dst}')

    transfer_log = sftp.put_by_list(**kwargs)

    assert transfer_log == log['transfers']
    assert sftp.log.as_dict() == log

    # Test existence and clean
    for dst_file in dst_list:
        logger.debug(f'Testing for file: {dst_file}')
        assert sftp.exists(dst_file)

    clean(dst_list)
