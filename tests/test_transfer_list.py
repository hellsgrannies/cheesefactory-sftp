# tests/transfer_list.py

import logging
from cheesefactory_sftp.transfer_list import CfSftpTransferList

logger = logging.getLogger(__name__)


def test_cfsftptransferlist():
    transfer_list = CfSftpTransferList()
    assert transfer_list.list == []


def test_cfsftptransferlist_properties():
    transfer_list = CfSftpTransferList()

    transfer_list.list = [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'},
    ]

    assert transfer_list.dst_list == [
        '/tmp_x/dst1.txt',
        'dst2.txt',
        '/dst3.txt',
        '/tmp_y/dir2/dst4.txt',
        '/tmp_x/dst5.txt'
    ]
    assert transfer_list.src_list == [
        'src1.txt',
        '/src2.txt',
        '/tmp_x/src3.txt',
        '/tmp_x/src4.txt',
        '/tmp_y/dir2/src5.txt'
    ]


def test_cfsftptransferlist_build_transfer_list_noflatten():
    file_list = ['/tmp_x/file1.txt', 'file2.txt', '/file3.txt', '/tmp_y/dir2/file4.txt', '/tmp_x/file5.txt']
    transfer_list = CfSftpTransferList.build_list(
        base_dst_dir='/base_dst/dst_dir',
        base_src_dir='/base_src/src_dir/',
        file_list=file_list,
    )
    assert transfer_list.list == [
        {
            'dst': '/base_dst/dst_dir//tmp_x/file1.txt',
            'src': '/base_src/src_dir//tmp_x/file1.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file2.txt',
            'src': '/base_src/src_dir/file2.txt'
        },
        {
            'dst': '/base_dst/dst_dir//file3.txt',
            'src': '/base_src/src_dir//file3.txt'
        },
        {
            'dst': '/base_dst/dst_dir//tmp_y/dir2/file4.txt',
            'src': '/base_src/src_dir//tmp_y/dir2/file4.txt'
        },
        {
            'dst': '/base_dst/dst_dir//tmp_x/file5.txt',
            'src': '/base_src/src_dir//tmp_x/file5.txt'
        }
    ]


def test_cfsftptransferlist_build_transfer_list_flatten():
    file_list = ['/tmp_x/file1.txt', 'file2.txt', '/file3.txt', '/tmp_y/dir2/file4.txt', '/tmp_x/file5.txt']
    transfer_list = CfSftpTransferList.build_list(
        base_dst_dir='/base_dst/dst_dir',
        base_src_dir='/base_src/src_dir/',
        file_list=file_list,
        flatten_dst=True,
        flatten_src=True
    )
    assert transfer_list.list == [
        {
            'dst': '/base_dst/dst_dir/file1.txt',
            'src': '/base_src/src_dir/file1.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file2.txt',
            'src': '/base_src/src_dir/file2.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file3.txt',
            'src': '/base_src/src_dir/file3.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file4.txt',
            'src': '/base_src/src_dir/file4.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file5.txt',
            'src': '/base_src/src_dir/file5.txt'
        }
    ]


def test_cfsftptransferlist_import_list():
    file_list = [
        {
            'dst': '/base_dst/dst_dir/file1.txt',
            'src': '/base_src/src_dir/file1.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file2.txt',
            'src': '/base_src/src_dir/file2.txt'
        },
        {
            'dst': '/base_dst/dst_dir/file3.txt',
            'src': '/base_src/src_dir/file3.txt'
        },
    ]
    transfer_list = CfSftpTransferList.import_list(
        file_list=file_list
    )

    assert transfer_list.list == [
        {'dst': '/base_dst/dst_dir/file1.txt', 'src': '/base_src/src_dir/file1.txt'},
        {'dst': '/base_dst/dst_dir/file2.txt', 'src': '/base_src/src_dir/file2.txt'},
        {'dst': '/base_dst/dst_dir/file3.txt', 'src': '/base_src/src_dir/file3.txt'}
    ]


def test_cfsftptransferlist_add_base_dst_dir():
    transfer_list = CfSftpTransferList()

    transfer_list.list = [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'},
    ]
    transfer_list.add_base_dst_dir('/yolo_dst/')
    assert transfer_list.list == [
        {'dst': '/yolo_dst//tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': '/yolo_dst/dst2.txt', 'src': '/src2.txt'},
        {'dst': '/yolo_dst//dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/yolo_dst//tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/yolo_dst//tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'}
    ]


def test_cfsftptransferlist_add_base_src_dir():
    transfer_list = CfSftpTransferList()

    transfer_list.list = [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'},
    ]
    transfer_list.add_base_src_dir('/yolo_src')
    assert transfer_list.list == [
        {'dst': '/tmp_x/dst1.txt', 'src': '/yolo_srcsrc1.txt'},
        {'dst': 'dst2.txt', 'src': '/yolo_src/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/yolo_src/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/yolo_src/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/yolo_src/tmp_y/dir2/src5.txt'}
    ]


def test_cfsftptransferlist_flatten_dst():
    transfer_list = CfSftpTransferList()

    transfer_list.list = [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'},
    ]
    transfer_list.flatten_dst()
    assert transfer_list.list == [
        {'dst': 'dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': 'dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': 'dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': 'dst5.txt', 'src': '/tmp_y/dir2/src5.txt'}
    ]


def test_cfsftptransferlist_flatten_src():
    transfer_list = CfSftpTransferList()

    transfer_list.list = [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': '/src2.txt'},
        {'dst': '/dst3.txt', 'src': '/tmp_x/src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': '/tmp_x/src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': '/tmp_y/dir2/src5.txt'},
    ]
    transfer_list.flatten_src()
    assert transfer_list.list == [
        {'dst': '/tmp_x/dst1.txt', 'src': 'src1.txt'},
        {'dst': 'dst2.txt', 'src': 'src2.txt'},
        {'dst': '/dst3.txt', 'src': 'src3.txt'},
        {'dst': '/tmp_y/dir2/dst4.txt', 'src': 'src4.txt'},
        {'dst': '/tmp_x/dst5.txt', 'src': 'src5.txt'}
    ]
