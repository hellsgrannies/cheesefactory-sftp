# test/test_connection.py

import pytest
import logging
from pathlib import Path
import socket
from cryptography.hazmat.primitives.asymmetric import rsa, dsa
from cryptography.hazmat.primitives import serialization
from cheesefactory_sftp.connection import CfSftpConnection

logger = logging.getLogger(__name__)

#
# Test config
#
SFTP_USER = 'testingtom'
SFTP_PASSWORD = 'tomspassword'
SFTP_HOST = 'cfsftp_sshd'  # Container hostname in docker-compose.yml
SFTP_PORT = 22


def test_sftp_port_open():
    # Is port 22 open on the server? It should be.
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = my_socket.connect_ex((SFTP_HOST, SFTP_PORT))
    assert result == 0, f'SFTP server ({SFTP_HOST}) not listening on port {str(SFTP_PORT)}'


@pytest.fixture(scope='module')
def sftp():
    cfsftp = CfSftpConnection.connect(
        host=SFTP_HOST,
        port=SFTP_PORT,
        user=SFTP_USER,
        password=SFTP_PASSWORD
    )
    yield cfsftp

    logger.debug("Teardown CfSftp instance.")
    cfsftp.close()  # Needed to prepare fixture for next test run.


#
# Properties
#

def test_status(sftp):
    # Does the connection status() return what is expected? It should.
    status = sftp.status
    assert status == (True, 'Transport open and authenticated.')


#
#  Class Methods
#

def test_connect(sftp):
    logger.debug('Testing connect() fixture.')
    assert sftp.log.as_dict() == {
        'connection_ok': True,
        'host': SFTP_HOST,
        'note': None,
        'password': SFTP_PASSWORD,
        'port': str(SFTP_PORT),
        'transfers': [],
        'user': SFTP_USER
    }
    assert sftp.log.as_string() == "{'connection_ok': True, 'host': 'cfsftp_sshd', 'note': None, " \
                                   "'password': 'tomspassword', 'port': '22', 'transfers': [], 'user': 'testingtom'}"


#
# Protected Methods
#

def test_get_key_rsa():
    cfsftp = CfSftpConnection()

    # Test key_path = non-existent file
    cfsftp.key_path = str(Path('/does/not.exist'))

    with pytest.raises(ValueError) as excinfo:
        cfsftp._get_key()
    assert f'key_path is not a valid file path: {str(cfsftp.key_path)}' in str(excinfo.value)

    # Make RSA key
    rsa_file = Path('rsa.key')
    # noinspection PyArgumentList
    rsa_private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048
    )
    pem = rsa_private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    with open(str(rsa_file), 'wb') as pem_out:
        pem_out.write(pem)

    # Test RSA key import

    cfsftp.key_path = str(rsa_file)
    cfsftp._get_key()

    # Clean up
    rsa_file.unlink()
    assert not Path('rsa.key').exists()


def test_get_key_dsa():
    cfsftp = CfSftpConnection()

    # Test key_path = non-existent file
    cfsftp.key_path = str(Path('/does/not.exist'))

    with pytest.raises(ValueError) as excinfo:
        cfsftp._get_key()
    assert f'key_path is not a valid file path: {str(cfsftp.key_path)}' in str(excinfo.value)

    # Make DSA key
    dsa_file = Path('dsa.key')
    # noinspection PyArgumentList
    dsa_private_key = dsa.generate_private_key(key_size=2048)
    pem = dsa_private_key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption()
    )

    with open(str(dsa_file), 'wb') as pem_out:
        pem_out.write(pem)

    # Test DSA key import
    cfsftp.key_path = str(dsa_file)
    cfsftp._get_key()

    # Clean up
    dsa_file.unlink()
    assert not Path('dsa.key').exists()


def test_start_transport():
    pass
