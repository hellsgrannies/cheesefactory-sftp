# log.py

import logging
import socket
from typing import List, Union

from cheesefactory_logger_postgres import CfLogPostgres
from cheesefactory_sftp.exceptions import BadListValueError

logger = logging.getLogger(__name__)


class CfSftpLogEntry:
    def __init__(self):
        # Fields and default values
        # self.id: int = None
        self.action: str = None
        self.action_ok: int = None
        self.client: str = None
        self.local_host: str = socket.gethostbyname(socket.gethostname())
        self.local_path: str = None
        self.log_local: bool = None
        self.log_remote: bool = None
        self.notes: str = None
        self.preserve_mtime: int = None
        self.preserve_mtime_ok: int = None
        self.redo: int = None
        self.remote_host: str = None
        self.remote_path: str = None
        self.remove_source: int = None
        self.remove_source_ok: int = None
        self.size: int = None
        self.size_match_ok: int = None
        self.status: int = 'STARTED'
        self.suffix: str = None
        self.suffix_ok: int = None
        self.mark: int = None
        self.timestamp: str = None

    def get(self):
        return {
            # 'id': self.id,  # Created by SQLite
            'action': self.action,
            'action_ok': self.action_ok,
            'client': self.client,
            'local_host': self.local_host,
            'local_path': self.local_path,
            'notes': self.notes,
            'preserve_mtime': self.preserve_mtime,
            'preserve_mtime_ok': self.preserve_mtime_ok,
            'redo': self.redo,
            'remote_host': self.remote_host,
            'remote_path': self.remote_path,
            'remove_source': self.remove_source,
            'remove_source_ok': self.remove_source_ok,
            'size': self.size,
            'size_match_ok': self.size_match_ok,
            'status': self.status,
            'suffix': self.suffix,
            'suffix_ok': self.suffix_ok,
            # 'mark': self.mark,
            # 'timestamp': self.timestamp  # Created by SQLite
        }


class CfSftpLogPostgres:
    def __init__(self):
        self._connection: CfLogPostgres = None
        self.schema = ''
        self.table = ''
        self.log_entry = {}  # Entry can be created directly here or will be filled in with values below before write.

        self.table_sql = {f"""
            CREATE TYPE {self.schema}.sftp_action_type AS ENUM ('GET', 'PUT');
            CREATE TYPE {self.schema}.sftp_status_type AS ENUM ('STARTED', 'OK', 'ERROR');
            CREATE TABLE IF NOT EXISTS {self.schema}.{self.table} (
                id SERIAL PRIMARY KEY NOT NULL,
                action {self.schema}.sftp_action_type,
                action_ok BOOLEAN,
                client TEXT,
                local_host TEXT,
                local_path TEXT,
                notes TEXT,
                preserve_mtime BOOLEAN,
                preserve_mtime_ok BOOLEAN,
                redo BOOLEAN,
                remote_host TEXT,
                remote_path TEXT,
                remove_source BOOLEAN,
                remove_source_ok BOOLEAN,
                size INT,
                size_match_ok BOOLEAN,
                status {self.schema}.sftp_status_type,
                suffix TEXT,
                suffix_ok BOOLEAN,
                datetime TIMESTAMP DEFAULT now()
            );
        """}

        # Fields and types
        self.field_list = {
            'id': 'INT',
            'action': 'sftp_action_type',
            'action_ok': 'BOOLEAN',
            'client': 'TEXT',
            'local_host': 'TEXT',
            'local_path': 'TEXT',
            'notes': 'TEXT',
            'preserve_mtime': 'BOOLEAN',
            'preserve_mtime_ok': 'BOOLEAN',
            'redo': 'BOOLEAN',
            'remote_host': 'TEXT',
            'remote_path': 'TEXT',
            'remove_source': 'BOOLEAN',
            'remove_source_ok': 'BOOLEAN',
            'size': 'INT',
            'size_match_ok': 'BOOLEAN',
            'status': 'sftp_status_type',
            'suffix': 'TEXT',
            'suffix_ok': 'BOOLEAN',
            'datetime': 'TIMESTAMP',
        }

    #
    # PUBLIC METHODS
    #

    def close(self):
        self._connection.close()

    def connect(self, host: str = '127.0.0.1', port: Union[str, int] = None, encoding: str = None, database: str = None,
                user: str = None, password: str = None, schema: str = 'public', table: str = 'log'):
        """Establish connection to Postgres server."""

        self.schema = schema
        self.table = table
        self._connection = CfLogPostgres.connect(host=host, port=port, database=database, user=user, password=password,
                                                 schema=schema, table=table, field_list=self.field_list)

    def exists(self, match_on: List[str], log_entry: CfSftpLogEntry = None) -> bool:
        """Does record exist?"""

        # If the file exists as matching local_path and remote_path in either log, don't move it.
        # Build the WHERE part of the query
        for value in match_on:
            if value not in ['local_host', 'local_path', 'remote_host', 'remote_path', 'size', 'status']:
                raise BadListValueError(f"Bad value found in 'skip_match_on': {value}\nValid values: 'local_host', "
                                        f"'local_path', 'remote_host', 'remote_path', 'size', 'status'")
        where = ''
        if 'local_host' in match_on:
            where = where + f"local_host = '{log_entry.local_host}' AND "
        if 'local_path' in match_on:
            where = where + f"local_path = '{log_entry.local_path}' AND "
        if 'remote_host' in match_on:
            where = where + f"remote_host = '{log_entry.remote_host}' AND "
        if 'remote_path' in match_on:
            where = where + f"remote_path = '{log_entry.remote_path}' AND "
        if 'size' in match_on:
            where = where + f"size = {log_entry.size} AND "
        if 'status' in match_on:
            where = where + f"status = 0 AND "
        where = where[:-5]  # Remove trailing "AND"
        logger.debug(f'skip_match_on WHERE: {where}')

        results = self._connection.read_records(where=where)

        if results is not None and len(results) > 0:
            # Do not transfer the file. Break out of this method.
            raise FileExistsError(f'Skipping file. Copied. local: {log_entry.local_host}:{log_entry.local_path}, '
                                  f'remote: {log_entry.remote_host}:{log_entry.remote_path}, size: {log_entry.size}, '
                                  f'status: {log_entry.status}')
        else:
            return False

    def write_log(self, pk: int = None, log_entry: CfSftpLogEntry = None) -> int:
        """Write log entry to database.

        Args:
            pk:
            log_entry:
        Returns:
            Primary key of the affected table record.
        """
        return self._connection.write_kwargs(pk=pk, **log_entry.get())
