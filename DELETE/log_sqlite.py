# log.py

import logging
import os
import socket
import tempfile
from pathlib import Path
from sqlite3 import OperationalError
from typing import List

import paramiko
from cheesefactory_logger_sqlite import CfLogSqlite
from cheesefactory_sftp.exceptions import BadListValueError

logger = logging.getLogger(__name__)


class CfSftpLogSqlite:
    def __init__(self):
        self.log_name = '.transfer.sqlite'

        self._local_connection = None
        self._local_path = None  # Path for local log
        self._remote_connection = None
        self._remote_path = None  # path for remote log
        self._sftp: paramiko.SFTPClient = None
        self._temp_dir: tempfile.TemporaryDirectory = None  # Temporary local path for the remote log
        self._temp_path = None  # Local temporary path for remote log
        
        self.fields = []
        
        # Fields and default values
        self.id: int = None
        self.action: str = None
        self.action_ok: int = None
        self.client: str = None
        self.local_host: str = socket.gethostbyname(socket.gethostname())
        self.local_path: str = None
        self.log_local: bool = None
        self.log_remote: bool = None
        self.notes: str = None
        self.preserve_mtime: int = None
        self.preserve_mtime_ok: int = None
        self.redo: int = None
        self.remote_host: str = None
        self.remote_path: str = None
        self.remove_source: int = None
        self.remove_source_ok: int = None
        self.size: int = None
        self.size_match_ok: int = None
        self.status: int = 0
        self.suffix: str = None
        self.suffix_ok: int = None
        self.mark: int = None
        self.timestamp: str = None

        # Fields and types
        self.field_types = {
            'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
            'action': 'TEXT',
            'action_ok': 'INTEGER',
            'client': 'TEXT',
            'local_host': 'TEXT',
            'local_path': 'TEXT',
            'log_local': 'INTEGER',
            'log_remote': 'INTEGER',
            'notes': 'TEXT',
            'preserve_mtime': 'INTEGER',
            'preserve_mtime_ok': 'INTEGER',
            'redo': 'INTEGER',
            'remote_host': 'TEXT',
            'remote_path': 'TEXT',
            'remove_source': 'INTEGER',
            'remove_source_ok': 'INTEGER',
            'size': 'INTEGER',
            'size_match_ok': 'INTEGER',
            'status': 'INTEGER',
            'suffix': 'TEXT',
            'suffix_ok': 'INTEGER',
            'mark': 'INTEGER',
            'timestamp': 'TEXT DEFAULT CURRENT_TIMESTAMP',
        }

    #
    # CLASS METHODS
    #

    @classmethod
    def connect(cls, log_local: bool = False, local_path: str = None,
                log_remote: bool = False, remote_path: str = None, sftp: paramiko.SFTPClient = None):
        log = cls()

        log.log_local = log_local
        log.local_path = local_path
        log.log_remote = log_remote
        log.remote_path = remote_path
        log._sftp = sftp

        # # Make it so local_path is not required and looks good, especially for logging (complete with parent dir).
        # logger.debug(f'Old local_path: {local_path}')
        # if local_path is None:
        #     local_path = f'{os.path.dirname(os.path.realpath(__file__))}/{Path(remote_path).name}'
        # if local_path[0] != '/':  # Get a parent directory if none given.
        #     local_path = str(Path(f'{os.path.dirname(os.path.realpath(__file__))}/{local_path}'))
        # logger.debug(f'New local_path: {local_path}')

        if log_local is True:
            if local_path is None:
                raise ValueError('log_local is True, but missing local_path.')

            local_log_dir = Path(local_path).parent  # Find the local log directory.
            Path(local_log_dir).mkdir(parents=True, exist_ok=True)  # Make sure it exists.
            log._local_path = f'{str(local_log_dir)}/{log.log_name}'  # Put together the local log path.
            log._local_connection = CfLogSqlite.connect(database_path=log._local_path, create=True,
                                                        field_list=log.field_types)
        if log_remote is True:
            if remote_path is None or sftp is None:
                raise ValueError('log_remote is True, but missing remote_path and/or sftp_connection.')

            try:
                log._temp_dir = tempfile.TemporaryDirectory(dir=os.path.dirname(os.path.abspath(__file__)))
                log._temp_path = f'{log._temp_dir.name}/{log.log_name}'  # Put together temp local path.
                log._remote_path = f'{str(Path(remote_path).parent)}/{log.log_name}'  # Put together remote log path.
                sftp.get(remotepath=log._remote_path, localpath=log._temp_path)  # Retrieve any existing log.

            except FileNotFoundError:
                logger.debug('Remote log file not found. Creating a new one.')

            finally:
                try:
                    log._remote_connection = CfLogSqlite.connect(database_path=log._temp_path, create=True,
                                                                 field_list=log.field_types)
                except OperationalError:
                    log._temp_dir.cleanup()  # If a problem happens while connecting. Do not leave a zombie temp_dir.
                    raise
        return log

    #
    # PUBLIC METHODS
    #
    
    def close(self):
        if self.log_local is True:
            self._local_connection.close()

        if self.log_remote is True:
            self._remote_connection.close()
            logger.debug(f'Uploading log file: {self._temp_path} -> {self._remote_path}')
            self._sftp.put(localpath=self._temp_path, remotepath=self._remote_path)

        if self._temp_dir is not None:
            self._temp_dir.cleanup()
            self._temp_dir = None
    
    def exists(self, match_on: List[str]) -> bool:
        """Does record exist?"""
        local_results = None
        remote_results = None
        
        if self.log_local is False and self.log_remote is False:
            raise ValueError('Logging not enabled. Unable to determine if the file exists in the logs.')

        # If the file exists as matching local_path and remote_path in either log, don't move it.
        # Build the WHERE part of the query
        for value in match_on:
            if value not in ['local_host', 'local_path', 'remote_host', 'remote_path', 'size', 'status']:
                raise BadListValueError(f"Bad value found in 'skip_match_on': {value}\nValid values: 'local_host', "
                                        f"'local_path', 'remote_host', 'remote_path', 'size', 'status'")
        where = ''
        if 'local_host' in match_on:
            where = where + f"local_host = '{self.local_host}' AND "
        if 'local_path' in match_on:
            where = where + f"local_path = '{self.local_path}' AND "
        if 'remote_host' in match_on:
            where = where + f"remote_host = '{self.remote_host}' AND "
        if 'remote_path' in match_on:
            where = where + f"remote_path = '{self.remote_path}' AND "
        if 'size' in match_on:
            where = where + f"size = {self.size} AND "
        if 'status' in match_on:
            where = where + f"status = 0 AND "
        where = where[:-5]  # Remove trailing "AND"
        logger.debug(f'skip_match_on WHERE: {where}')

        if self.log_local is True:
            local_results = self._local_connection.read_records(where=where)
        if self.log_remote is True:
            remote_results = self._remote_connection.read_records(where=where)

        if (local_results is not None and len(local_results) > 0) \
                or (remote_results is not None and len(remote_results)) > 0:
            # Do not transfer the file. Break out of this method.
            raise FileExistsError(f'Skipping file. Copied. local: {self.local_host}:{self.local_path}, remote: '
                                  f'{self.remote_host}:{self.remote_path}, size: {self.size}, status: {self.status}')
        else:
            return None

    def write_log(self):
        log = {
            # 'id': self.id,  # Created by SQLite
            'action': self.action,
            'action_ok': self.action_ok,
            'client': self.client,
            'local_host': self.local_host,
            'local_path': self.local_path,
            'log_local': int(self.log_local),
            'log_remote': int(self.log_remote),
            'notes': self.notes,
            'preserve_mtime': self.preserve_mtime,
            'preserve_mtime_ok': self.preserve_mtime_ok,
            'redo': self.redo,
            'remote_host': self.remote_host,
            'remote_path': self.remote_path,
            'remove_source': self.remove_source,
            'remove_source_ok': self.remove_source_ok,
            'size': self.size,
            'size_match_ok': self.size_match_ok,
            'status': self.status,
            'suffix': self.suffix,
            'suffix_ok': self.suffix_ok,
            'mark': self.mark,
            # 'timestamp': self.timestamp  # Created by SQLite
        }
        
        if self.log_local is True:
            self._local_connection.write_kwargs(**log)
        if self.log_remote is True:
            self._remote_connection.write_kwargs(**log)
