import logging
import sqlite3
import re
import os
from datetime import datetime
from pathlib import Path

logger = logging.getLogger(__name__)


class CfSftpLog:
    def __init__(self):
        """Wrapper for SQLite. Used to log SFTP file transactions.

        Notes:
            CfSftp --> CfSftpGet ----> CfSftpTransfer --> CfSftpUtilities --> CfSftpConnection
                   |-> CfSftpPut ->|                  |-> CfSftpLog (import)
        """
        self._log_dir: str = None
        self._log_file: str = '.transfer.sqlite'
        self.last_archive_name: str = None
        self.table = 'log'

        # TODO: Add size?
        self.field_list = {
            'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
            'action': 'TEXT',
            'action_ok': 'INTEGER',
            'client': 'TEXT',
            'local_path': 'TEXT',
            'notes': 'TEXT',
            'preserve_mtime': 'INTEGER',
            'preserve_mtime_ok': 'INTEGER',
            'redo': 'INTEGER',
            'remote_host': 'TEXT',
            'remote_path': 'TEXT',
            'remove_source': 'INTEGER',
            'remove_source_ok': 'INTEGER',
            'size': 'INTEGER',
            'size_match_ok': 'INTEGER',
            'status': 'INTEGER',
            'suffix': 'TEXT',
            'suffix_ok': 'INTEGER',
            'timestamp': 'TEXT DEFAULT CURRENT_TIMESTAMP',
        }

        self._connection = None
        self._cursor = None

    #
    # PROPERTIES
    #

    @property
    def last_archive_path(self) -> str:
        return str(Path(self.log_dir, self.last_archive_name))

    @property
    def log_dir(self) -> str:
        return self._log_dir

    @log_dir.setter
    def log_dir(self, value):
        if value is None:
            self._log_dir = os.path.dirname(os.path.abspath(__file__))
        else:
            self._log_dir = str(Path(value))

    @property
    def log_file(self) -> str:
        return self._log_file

    @log_file.setter
    def log_file(self, value):
        if value is None:
            self._log_file = '.transfer.sqlite'
        else:
            self._log_file = str(value).strip('\\/')

    @property
    def log_path(self) -> str:
        if self._log_dir is None:
            self.log_dir = None  # Set a valid value
        return str(Path(self._log_dir, self._log_file))

    @property
    def log_status(self):
        """Retrieve connection status.

        status() for cheesefactory programs should return True/False along with any reasons.

        Returns:
            True, if live. False, if not live.
            Additional info, such as error codes. (not implemented)
        """
        try:
            self._cursor.execute('SELECT sqlite_version();')
            result = self._cursor.fetchall()
            version = result[0][0]

            if re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', version):
                return True, 'None'
        except AttributeError:
            return False, 'No connection detected.'
        else:
            return False, 'No connection detected.'

    #
    # Class Methods
    #

    @classmethod
    def log_connect(cls, log_file: str = None, log_dir: str = None):
        """
        Args:
            log_file: Name of the SQLite database. Default is '.transfer.sqlite'
            log_dir:
        Returns:
            An instance of CfSftpLog
        """
        log = cls()

        log.log_file = log_file
        log.log_dir = log_dir

        if Path(log.log_path).exists():
            logger.debug(f'Database exists: {log.log_path}')
            # A database file exists. Connect to it.
            log._connection = sqlite3.connect(log.log_path)
            log._cursor = log._connection.cursor()
            try:
                # Test database
                log._test_log()
                logger.debug(f'Connected to existing database: {log.log_path}')
            except ValueError:
                # Database tested badly. Archive, recreate and retest.
                logger.warning(f'Database structure problem.')
                log.archive_log()
                logger.debug(f'Recreating database.')
                log._connection = sqlite3.connect(log.log_path)
                log._cursor = log._connection.cursor()
                log._create_table()
                log._test_log()
                logger.debug(f'Connected to new database: {log.log_path}')
        else:
            # Create a new database file. Make table and test.
            logger.debug(f'Connecting to new database: {log.log_path}')
            log._connection = sqlite3.connect(log.log_path)
            log._cursor = log._connection.cursor()
            log._create_table()
            log._test_log()
            logger.debug('Connected.')

        return log

    #
    # Protected Methods
    #

    def _create_table(self):
        """Create a log table."""
        sql = f'CREATE TABLE {self.table} ('
        for field, field_type in self.field_list.items():
            sql += f' {field} {field_type},'
        sql = sql[:-1]  # Erase trailing comma
        sql += ');'
        logger.debug(f'Creating table ({self.table}): {sql}')
        self._cursor.execute(sql)
        logger.debug(f'New table created: {self.table}')

    def _test_log(self):
        """Check database sanity."""
        # Does the log table exist in the database?
        self._cursor.execute(f"SELECT name FROM main.sqlite_master WHERE type = 'table' AND name = '{self.table}';")
        results = self._cursor.fetchall()

        if len(results) == 0:
            raise ValueError(f'Missing table: {self.table}')

        # Build a list of fields present in the table
        self._cursor.execute(f"PRAGMA table_info({self.table});")
        results = self._cursor.fetchall()
        table_fields = []
        for result in results:
            table_fields.append(result[1])

        # Does the table contain all of the required fields?
        missing_fields = []
        for field in self.field_list.keys():
            if field not in table_fields:
                missing_fields.append(field)
        if len(missing_fields) > 0:
            raise sqlite3.Error(f'Table {self.table} is missing required fields: {", ".join(missing_fields)}.')
        logger.debug(f'Table successfully tested: {self.table}')

    #
    # Public Methods
    #

    def archive_log(self) -> str:
        """Close any existing connection and move the database.

        Returns:
            New file name.
        """
        append_date = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S')
        self.last_archive_name = f'{self.log_file}.archive.{append_date}'
        self.move_log(self.last_archive_name)
        logger.debug(f'Database archived: {self.log_file} -> {self.last_archive_name}')

    def close(self):
        """Close the SQLite connection, if it is open."""
        if self.log_status[0] is True:
            self._connection.close()

    def move_log(self, new_name=None):
        """Close any existing connection and move the database."""
        if new_name is None:
            raise ValueError('No new_name defined. Nothing to move to!')
        if self._log_dir is None:
            raise ValueError('Trying to move log_file without first defining log_dir.')

        self.close()  # Kill connection to file, if it exists.
        new_path = Path(self.log_dir, new_name)
        Path(self.log_path).rename(str(new_path))

        if not new_path.exists():
            raise FileNotFoundError(f'Could not create new file when renaming: {self.log_path} -> {str(new_path)}')
        if Path(self.log_path).exists():
            raise FileExistsError(f'New file created, but old file still exists: {self.log_path} -> {str(new_path)}')

        logger.debug(f'Database file name changed: {self.log_path} -> {str(new_path)}')

    def read(self, days=1, where=None):
        """Produce all log entries X days back.

        Args:
            days: How many days ago to start reading the logs from?
            where: SQL WHERE to filter data.
        """
        sql = f"SELECT * FROM log WHERE timestamp > datetime('now', '-{days} day', 'localtime')"

        if where is not None:
            sql += ' AND {where}'

        logger.debug(sql)
        self._cursor.execute(sql)
        results = self._cursor.fetchall()
        logger.debug(results)
        return results

    def write(self, pk: int = None, **fields) -> int:
        """Insert record into database regarding attempted and successful file transactions.

        Args:
            pk: Table record ID (the primary key). If included, UPDATES an existing row.
            fields: A list of fields to update. Usable fields keys:
                client: How is this log being called? (manual, CfSftp, etc.)
                remote_host: Host or IP of remote host.
                local_path: Local full path of file or directory.
                remote_path: Remote full path of file or directory.
                postfix: String appended to file name after successful action.
                action: 'get' or 'put'.
                status: 'success' or 'fail'
                notes: Additional information regarding the transaction.

        Returns:
            SQLite's lastrowid -- the primary key of the affected row.
        """
        # Are all given fields present in self.field_list? If not, raise an error.
        field_map = ''  # Prep for possible UPDATE. So fields don't need to be traversed again.
        ordered_keys = ''
        ordered_values = ''
        invalid_keys = ''
        field_list = self.field_list.keys()

        for key, value in fields.items():
            if key not in field_list:
                invalid_keys += f'{key}, '
            if key == 'id':  # If primary key, skip
                continue
            elif isinstance(value, int):
                field_map += f'{key} = {str(value)}, '
                ordered_keys += f'{key}, '
                ordered_values += f'{str(value)}, '
            elif isinstance(value, str):
                field_map += f"{key} = '{value}', "
                ordered_keys += f'{key}, '
                ordered_values += f"'{str(value)}', "

        if invalid_keys != '':
            invalid_keys = invalid_keys[:-2]  # Clean up the trailing commas and whitespaces
            raise ValueError(f'Invalid key(s): {invalid_keys}. '
                             f'Acceptable values are {", ".join(field_list)}')

        # Determine if this is an INSERT or UPDATE: Has "id" been populated with a primary key?
        if pk is None:  # New record. Do an INSERT.
            ordered_keys = ordered_keys[:-2]  # Clean up the trailing comma and whitespaces
            ordered_values = ordered_values[:-2]  # Clean up the trailing comma and whitespaces
            sql = f'INSERT INTO {self.table} ({ordered_keys}) VALUES ({ordered_values});'
            logger.debug(f'write(): {sql}')
            self._cursor.execute(sql)
            self._connection.commit()
            pk = self._cursor.lastrowid

        else:  # Existing record. Do an UPDATE.
            field_map = field_map[:-2]  # Clean up the trailing comma and whitespaces
            # noinspection SqlResolve
            sql = f'UPDATE {self.table} SET {field_map} WHERE id = {pk};'
            logger.debug(f'write(): {sql}')
            self._cursor.execute(sql)
            self._connection.commit()

        return pk
