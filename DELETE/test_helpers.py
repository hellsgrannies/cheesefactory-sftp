import pytest
import logging
import warnings
from cheesefactory_sftp import helpers
from cheesefactory_sftp.exceptions import FileSizeMismatchError

logging.captureWarnings(True)


def test_deprecation(caplog):
    with warnings.catch_warnings(record=True) as w:
        warnings.simplefilter("always")  # Cause all warnings to always be triggered.
        helpers.deprecation('This is a test')  # Trigger a warning.
        # Verify some things
        assert len(w) == 1
        assert issubclass(w[-1].category, DeprecationWarning)
        assert 'This is a test' in str(w[-1].message)


def test_file_size_match():
    # Do we get a FileSizeMismatchError when the file sizes do not match? We should.
    with pytest.raises(FileSizeMismatchError) as excinfo:
        helpers.file_size_match(local_path='/home/testingtom/.bashrc', remote_path='/home/testingtom/dummy.txt',
                                remote_size=34)
    assert 'Size mismatch -- Local: /home/testingtom/.bashrc (3771) != Remote: /home/testingtom/dummy.txt (34)' \
           in str(excinfo.value)

    # Do we get a FileSizeMismatchError when the file sizes match? We shouldn't.
    helpers.file_size_match(local_path='/home/testingtom/.bashrc', remote_path='/home/testingtom/anyfile.txt',
                            remote_size=3771)


@pytest.mark.parametrize(
    'octal,decimal',
    [(777, 511), (600, 384), (3432424, 931092), (0, 0)]
)
def test_octal_to_decimal(octal, decimal):
    assert helpers.octal_to_decimal(octal) == decimal


@pytest.mark.parametrize(
    'st_mode,octal',
    [(777, 411), (600, 130), (3432424, 750), (100, 144)]
)
def test_st_mode_to_octal(st_mode, octal):
    assert helpers.st_mode_to_octal(st_mode) == octal
